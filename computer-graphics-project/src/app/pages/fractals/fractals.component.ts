import { Component } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {RouterLink} from "@angular/router";
import {BackButtonComponent} from "../../components/buttons/back-button/back-button.component";
import {FractalTypeSelectorComponent} from "../../components/selects/fractal-type-selector/fractal-type-selector.component";
import {CreateButtonComponent} from "../../components/buttons/create-button/create-button.component";
import {CanvasDrawingComponent} from "../../components/canvases/canvas-drawing/canvas-drawing.component";
import {ClearButtonComponent} from "../../components/buttons/clear-button/clear-button.component";
import {FigureTypeSwitchComponent} from "../../components/selects/figure-type-switch/figure-type-switch.component";
import {IterationsInputComponent} from "../../components/inputs/iterations-input/iterations-input.component";
import { RelationInputComponent } from '../../components/inputs/relation-input/relation-input.component';
import {SidePanelComponent} from "../../components/blocks/side-panel/side-panel.component";
import {Relation} from "../../models/relation";
import {SaveButtonComponent} from "../../components/buttons/save-button/save-button.component";
import { EducationalMaterialsButtonComponent } from 'src/app/components/buttons/educational-materials-button/educational-materials-button.component';

@Component({
  selector: 'app-fractals',
  standalone: true,
  imports: [CommonModule, RouterLink, BackButtonComponent, FractalTypeSelectorComponent, NgOptimizedImage, CreateButtonComponent, CanvasDrawingComponent, ClearButtonComponent, FigureTypeSwitchComponent, IterationsInputComponent, RelationInputComponent, SidePanelComponent, SaveButtonComponent, EducationalMaterialsButtonComponent],
  templateUrl: './fractals.component.html',
  styleUrls: ['./fractals.component.css']
})
export class FractalsComponent {
  iterations: number = 0;
  relation: Relation = new Relation(1, 1);

  onIterationValueChange(value: number) {
    this.iterations = value;
  }

  onRelationValueChange(relation: Relation) {
    this.relation = relation;
  }
}
