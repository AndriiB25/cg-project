import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterLink} from "@angular/router";
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { BackButtonComponent } from 'src/app/components/buttons/back-button/back-button.component';

@Component({
  selector: 'app-second-educational',
  standalone: true,
  imports: [CommonModule,BackButtonComponent,RouterLink,FontAwesomeModule],
  templateUrl: './second-educational.component.html',
  styleUrls: ['./second-educational.component.css']
})
export class SecondEducationalComponent {
  backButtonColor: string = '#00758A'; // Ваш вибір кольору
  faArrowLeft = faArrowLeft ;
}
