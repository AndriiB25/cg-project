import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterLink} from "@angular/router";
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { BackButtonComponent } from 'src/app/components/buttons/back-button/back-button.component';

@Component({
  selector: 'app-first-educational',
  standalone: true,
  imports: [CommonModule,BackButtonComponent,RouterLink,FontAwesomeModule],
  templateUrl: './first-educational.component.html',
  styleUrls: ['./first-educational.component.css']
})
export class FirstEducationalComponent {
  backButtonColor: string = '#00758A';
  faArrowRight = faArrowRight ;
}
