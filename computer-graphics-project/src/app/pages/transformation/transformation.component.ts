import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BackButtonComponent} from "../../components/buttons/back-button/back-button.component";
import {SidePanelComponent} from "../../components/blocks/side-panel/side-panel.component";
import { VertexInputComponent } from 'src/app/components/inputs/vertex-input/vertex-input.component';
import { LengthAltitudeComponent } from 'src/app/components/inputs/length-altitude-input/length-altitude-input.component';
import { LineParametersInputComponent } from 'src/app/components/inputs/line-parametrs-input/line-parameters-input.component';
import {ClearButtonComponent} from "../../components/buttons/clear-button/clear-button.component";
import {CreateButtonComponent} from "../../components/buttons/create-button/create-button.component";
import {SaveButtonComponent} from "../../components/buttons/save-button/save-button.component";
import {
  TransformationCanvasComponent
} from "../../components/canvases/transformation-canvas/transformation-canvas.component";
import { EducationalMaterialsButtonComponent } from 'src/app/components/buttons/educational-materials-button/educational-materials-button.component';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-transformation',
  standalone: true,
  imports: [CommonModule, BackButtonComponent, SidePanelComponent, TransformationCanvasComponent, VertexInputComponent, LengthAltitudeComponent, LineParametersInputComponent, ClearButtonComponent, CreateButtonComponent, SaveButtonComponent, EducationalMaterialsButtonComponent, RouterLink],
  templateUrl: './transformation.component.html',
  styleUrls: ['./transformation.component.css']
})
export class TransformationComponent {

}
