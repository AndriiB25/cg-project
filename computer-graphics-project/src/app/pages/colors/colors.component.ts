import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BackButtonComponent} from "../../components/buttons/back-button/back-button.component";
import {SidePanelComponent} from "../../components/blocks/side-panel/side-panel.component";
import {ColoredCanvasComponent} from "../../components/canvases/colored-canvas/colored-canvas.component";
import {ChooseImageButtonComponent} from "../../components/buttons/choose-image-button/choose-image-button.component";
import {ClearButtonComponent} from "../../components/buttons/clear-button/clear-button.component";
import {CreateButtonComponent} from "../../components/buttons/create-button/create-button.component";
import {SaveButtonComponent} from "../../components/buttons/save-button/save-button.component";
import { ColorSchemesOutputComponent } from 'src/app/components/outputs/color-schemes-output/color-schemes-output.component';
import { SaturationSliderComponent } from 'src/app/components/inputs/saturation-slider/saturation-slider.component';
import { FragmentSectionInputComponent } from 'src/app/components/inputs/fragment-section-input/fragment-section-input.component';
import { EducationalMaterialsButtonComponent } from 'src/app/components/buttons/educational-materials-button/educational-materials-button.component';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-colors',
  standalone: true,
  imports: [CommonModule, BackButtonComponent, SidePanelComponent, ColoredCanvasComponent, ChooseImageButtonComponent, ClearButtonComponent, CreateButtonComponent, SaveButtonComponent, ColorSchemesOutputComponent, SaturationSliderComponent, FragmentSectionInputComponent, EducationalMaterialsButtonComponent, RouterLink],
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.css']
})
export class ColorsComponent {
}
