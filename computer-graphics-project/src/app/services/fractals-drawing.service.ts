import {Injectable} from '@angular/core';
import {Complex} from "../models/complex";

@Injectable({
  providedIn: 'root'
})
export class FractalsDrawingService {
  private points : any[] = [];
  private relation : number;

  constructor() { }

  public drawUsingTangentOrCotangent(useTangent: boolean, ctx : CanvasRenderingContext2D, width: number, height: number, iterations: number) {
    let minRe = -5, maxRe = 5, minIm = -5, maxIm = 5;
    let reStep = (maxRe - minRe) / width, imStep = (maxIm - minIm) / height;
    let re = minRe;
    while (re < maxRe) {
      let im = minIm;
      while (im < maxIm) {
        let result = this.belongsToFractal(useTangent, re, im, iterations);
        let x = (re - minRe) / reStep, y = (im - minIm) / imStep;
        if (result === iterations) {
          this.drawPixel(ctx, x, y, 'black');
        } else {
          let h = 30 + Math.round(120 * result / iterations);
          let color = 'hsl(' + h + ', 100%, 50%)';
          this.drawPixel(ctx, x, y, color)
        }
        im += imStep;
      }
      re += reStep;
    }
  }

  public drawKoch(it: number, sides: number, relation: number, ctx: CanvasRenderingContext2D, width: number, height: number) {
    this.initialize(sides, relation, width, height);
    for (let i = 0; i < it; i++) {
      this.iterationCircle();
    }

    this.draw(ctx, width, height);
  }

  private initialize(sides: number, relation: number, width: number, height: number) {
    const centerX = width * 4 / 7, centerY = height / 2;
    const edge = width / 6;
    const edgeScale = 1;
    const globalRotation = 30;
    const centerDX = 0;
    const centerDY = 0;
    let _edge = edge * edgeScale;
    let alpha = 2 * Math.PI / sides;
    let rot = globalRotation * Math.PI / 180;
    let xc = centerX + centerDX;
    let yc = centerY + centerDY;
    let tempPoints = [];
    for (let i = 0; i < sides; i++) {
      tempPoints.push({
        x: xc + Math.cos(i * alpha + rot) * _edge,
        y: yc + Math.sin(i * alpha + rot) * _edge
      });
    }
    tempPoints.push(tempPoints[0]);
    this.points = tempPoints;
    this.relation = relation;
  }
  private iterationCircle() {
    let iter = this.points.length - 2;
    for (let j = iter; j >= 0; j--) {
      this.singleIteration(j);
    }
  }

  private calculateRelation(relationValue: number) {
    // Встановлюємо межі вхідних і вихідних значень
    const inputMin = 0; // Мінімальне значення на вході
    const inputMax = 0.5; // Максимальне значення на вході
    const outputMin = 3; // Мінімальне значення на виході
    const outputMax = 0; // Максимальне значення на виході
    if (parseFloat(relationValue.toFixed(2)) > inputMax) {
      relationValue = inputMax;
    } else if(parseFloat(relationValue.toFixed(2)) < inputMin){
      relationValue = inputMin;
    }

    // Масштабуємо вхідне значення до вихідного діапазону
    return (relationValue - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin;
  }

  private singleIteration(index: number) {
    let spikeBaseWidth = this.calculateRelation(this.relation);
    let l = Math.sqrt(
      (this.points[index + 1].x - this.points[index].x) *
      (this.points[index + 1].x - this.points[index].x) +
      (this.points[index + 1].y - this.points[index].y) *
      (this.points[index + 1].y - this.points[index].y)
    ) / 3;

    let dx = (this.points[index + 1].x - this.points[index].x) * (3 - spikeBaseWidth) / 6;
    let dy = (this.points[index + 1].y - this.points[index].y) * (3 - spikeBaseWidth) / 6;

    let mid1 = {
      x: this.points[index].x + dx,
      y: this.points[index].y + dy
    };
    let mid2 = {
      x: this.points[index + 1].x - dx,
      y: this.points[index + 1].y - dy
    };

    this.points.splice(index + 1, 0, mid2);
    this.points.splice(index + 1, 0, mid1);
        //divide central in half
    let cen1 = this.points[index + 1];
    let cen2 = this.points[index + 2];
    let mid = {
      x: (cen1.x + cen2.x) / 2,
      y: (cen1.y + cen2.y) / 2
    };

    let elevation = 1, rotation = 0;
    //shift central point
    l = l * elevation * Math.sqrt(3) / 2;
    let a = Math.atan((cen2.y - cen1.y) / (cen2.x - cen1.x));
    const angle = a + Math.PI / 2 + rotation * Math.PI / 180;
    let mid_s = {
      x: mid.x - Math.cos(angle) * l,
      y: mid.y - Math.sin(angle) * l
    };

    if (cen2.x < cen1.x)
      mid_s = {
      x: mid.x + Math.cos(angle) * l,
        y: mid.y + Math.sin(angle) * l
    };

    this.points.splice(index + 2, 0, mid_s);
  }

  private draw(context : CanvasRenderingContext2D, width: number, height: number) {
    let i;
    const edgeThickness = 1, dotThickness = 0;
    context.clearRect(0, 0, width, height);
    context.lineWidth = edgeThickness;
    for (i = 0; i < this.points.length - 1; i++) {
      if (edgeThickness > 0) {
        context.beginPath();
        context.moveTo(this.points[i].x, this.points[i].y);
        context.lineTo(this.points[i + 1].x, this.points[i + 1].y);
        context.stroke();
      }
    }
    for (i = 0; i < this.points.length - 1; i++) {
      if (dotThickness > 0) {
        if (i === 0) {
          context.beginPath();
          context.arc(this.points[i].x, this.points[i].y, dotThickness, 0, Math.PI * 2, true);
          context.fill();
        }
        context.beginPath();
        context.arc(this.points[i + 1].x, this.points[i + 1].y, dotThickness, 0, Math.PI * 2, true);
        context.fill();
      }
    }
  }

  private drawPixel(ctx: CanvasRenderingContext2D, x: number, y: number, color: string)
  {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, 1, 1);
  }

  private belongsToFractal(useTangent: boolean, re: number, im: number, iterations: number) {
    let z = new Complex(re, im);
    let c = new Complex(1, 0);
    let i = 0;
      while (z.abs() < 10 && i < iterations) {
        z = useTangent ?  z.tan() : z.cot();
        z = z.mul(z);
        z = z.mul(c);
        i++;
      }
    return i;
  }
}
