import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ResetValuesService {
  private defaultValueSubject = new BehaviorSubject<number>(0); // Initialize with an empty string
  clearInput$ = this.defaultValueSubject.asObservable();

  reset(value: number) {
    this.defaultValueSubject.next(value);
  }

  constructor() { }
}
