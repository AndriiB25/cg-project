import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {SchemesOutputData} from "../models/schemes-output-data";
import {Fragment} from "../models/fragment";

@Injectable({
  providedIn: 'root'
})
export class ColorSchemesService {
  private schemesDataSubject = new BehaviorSubject<SchemesOutputData>(new SchemesOutputData("", ""));
  schemesData$ = this.schemesDataSubject.asObservable();

  setOutputData(data: SchemesOutputData) {
    this.schemesDataSubject.next(data);
  }

  constructor() { }

  private hsv_to_rgb(h: number, s: number, v: number) {
    // Переводим h з діапазону [0, 360) до [0, 1]
    h /= 360;

    // Переводим s та v з діапазону [0, 100] до [0, 1]
    s /= 100;
    v /= 100;

    // Визначаємо значення проміжних змінних
    let c = v * s;
    let x = c * (1 - Math.abs((h * 6) % 2 - 1));
    let m = v - c;

    let r, g, b;

    if (0 <= h && h < 1/6) {
        r = c;
        g = x;
        b = 0;
    } else if (1/6 <= h && h < 2/6) {
        r = x;
        g = c;
        b = 0;
    } else if (2/6 <= h && h < 3/6) {
        r = 0;
        g = c;
        b = x;
    } else if (3/6 <= h && h < 4/6) {
        r = 0;
        g = x;
        b = c;
    } else if (4/6 <= h && h < 5/6) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }

    // Переводим значення RGB з [0, 1] до [0, 255]
    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return {
        r: r,
        g: g,
        b: b
    };
}

  rgb_to_hsv(r: number , g : number, b: number) {

    // R, G, B values are divided by 255
    // to change the range from 0..255 to 0..1
    r = r / 255.0;
    g = g / 255.0;
    b = b / 255.0;

    // h, s, v = hue, saturation, value
    let cmax = Math.max(r, Math.max(g, b)); // maximum of r, g, b
    let cmin = Math.min(r, Math.min(g, b)); // minimum of r, g, b
    let diff = cmax - cmin; // diff of cmax and cmin.
    let h = -1, s;

    // if cmax and cmax are equal then h = 0
    if (cmax == cmin)
        h = 0;

    // if cmax equal r then compute h
    else if (cmax == r)
        h = (60 * ((g - b) / diff) + 360) % 360;

    // if cmax equal g then compute h
    else if (cmax == g)
        h = (60 * ((b - r) / diff) + 120) % 360;

    // if cmax equal b then compute h
    else if (cmax == b)
        h = (60 * ((r - g) / diff) + 240) % 360;

    // if cmax equal zero
    if (cmax == 0)
        s = 0;
    else
        s = (diff / cmax) * 100;

    // compute v
    let v = cmax * 100;

    return {
      h: h,
      s: s,
      v: v,
    };
  }

  getRgbPixel(canvas: HTMLCanvasElement, mouseX: number, mouseY: number) {
    let imageData = canvas.getContext("2d")?.getImageData(0, 0, canvas.width, canvas.height).data;
    if(imageData) {
        let basePos = mouseY * canvas.width + mouseX;
        basePos *= 4;
        return {
            r: imageData[basePos],
            g: imageData[basePos + 1],
            b: imageData[basePos + 2]
        };
    } else {
      return {
          r: 0,
          g: 0,
          b: 0
      }
    }
  }


  rgb_to_cmyk(r : number, g : number, b : number) {
    // Переводим R, G, B в діапазон [0, 1]
    r = r / 255;
    g = g / 255;
    b = b / 255;

    // Обчислюємо значення CMY
    let c = 1 - r;
    let m = 1 - g;
    let y = 1 - b;

    // Знаходимо K (Key/Black)
    let k = Math.min(c, m, y);

    // Обчислюємо значення CMYK
    if (k === 1) {
        // Виняток: якщо K = 1, то CMY = 0
        c = 0;
        m = 0;
        y = 0;
    } else {
        c = (c - k) / (1 - k);
        m = (m - k) / (1 - k);
        y = (y - k) / (1 - k);
    }

    // Перетворюємо значення CMYK в відсоткову форму
    c = Math.round(c * 100);
    m = Math.round(m * 100);
    y = Math.round(y * 100);
    k = Math.round(k * 100);

    return {
        c: c,
        m: m,
        y: y,
        k: k
    };
  }

  private cmyk_to_rgb(c : number, m : number, y : number, k : number) {
    // Переводим значення CMYK з відсотків в дроби
    c = c / 100;
    m = m / 100;
    y = y / 100;
    k = k / 100;

    // Обчислюємо значення R, G, B
    let r = 255 * (1 - c) * (1 - k);
    let g = 255 * (1 - m) * (1 - k);
    let b = 255 * (1 - y) * (1 - k);

    // Округлюємо значення R, G, B до цілих чисел
    r = Math.round(r);
    g = Math.round(g);
    b = Math.round(b);

    return {
        r: r,
        g: g,
        b: b
    };
  }

  changeColors(originalCanvas: HTMLCanvasElement, fragment: Fragment, currentSaturation: number) {
    //console.log("Prev sat: " + currentSaturation + "Cur sat: "+ currentSaturation)
    let width = fragment.width, height = fragment.height, x = fragment.x, y = fragment.y;
    // @ts-ignore
    let imageData = originalCanvas.getContext("2d")
        .getImageData(x, y, width, height).data;

    for (let i = 0; i < width; ++i) {
      for (let j = 0; j < height; ++j) {
        let pos = (i * width + j) * 4;

        let pixel = {
          r: imageData[pos],
          g: imageData[pos + 1],
          b: imageData[pos + 2],
        };

        let pixelHsv = this.rgb_to_hsv(pixel.r, pixel.g, pixel.b);
        if (this.checkHueTolerance(pixelHsv.h)) {
          pixelHsv.s = currentSaturation;

          let pixelRgbFromHsv = this.hsv_to_rgb(pixelHsv.h, pixelHsv.s, pixelHsv.v);

          let pixelCmyk = this.rgb_to_cmyk(pixelRgbFromHsv.r, pixelRgbFromHsv.g, pixelRgbFromHsv.b);

        let pixelRgb = this.cmyk_to_rgb(pixelCmyk.c, pixelCmyk.m, pixelCmyk.y, pixelCmyk.k);
        imageData[pos] = pixelRgb.r;
        imageData[pos + 1] = pixelRgb.g;
        imageData[pos + 2] = pixelRgb.b;
        }
      }
    }

    // @ts-ignore
    originalCanvas.getContext("2d")
      .putImageData(new ImageData(imageData, width, height), x, y);
  }


  private checkHueTolerance(currentHue: number) {
    //console.log(currentHue);
    const HueOfColorGreen = 120;
    const HueOfColorMagenta = 300;
    let tolerance = 60;

    if (
      currentHue < HueOfColorGreen + tolerance &&
      currentHue > HueOfColorGreen
    ) {
      return true;
    }
    return currentHue < HueOfColorMagenta + tolerance &&
      currentHue > HueOfColorMagenta;
  }
}

