import { Injectable } from '@angular/core';
import {Vertex} from "../models/vertex";

@Injectable({
  providedIn: 'root'
})
export class FiguresDrawingService {

  constructor() { }

  // Побудова рівнобедреного трикутника
  buildIsoscelesTriangle(firstVertex: Vertex, secondVertex: Vertex, altitudeLength: number) : [Vertex[], [number[], number[]]] {
    let result = [];
    result.push(firstVertex, secondVertex);

    // Знаходження серединної точки між вершинами основи
    const midPoint: Vertex = {
      x: (firstVertex.x + secondVertex.x) / 2,
      y: (firstVertex.y + secondVertex.y) / 2
    };

    // Знаходження кута нахилу між вершинами основи
    const angle = Math.atan2(secondVertex.y - firstVertex.y, secondVertex.x - firstVertex.x);

    // Знаходження координат третьої вершини
    const thirdVertex: Vertex = {
      x: midPoint.x - altitudeLength * Math.sin(angle),
      y: midPoint.y + Math.abs(altitudeLength * Math.cos(angle))
    };

    result.push(thirdVertex);

    return [result, this.prepareTriangleData(result)];
  }

  // побудова прямої Ax + By + C = 0
  buildLine(A :number, B: number, C: number): [number[], number[]] {
    let xValues: number[] = [];
    xValues.push(-10000,10000);

    // Знаходження відповідних значень y за рівнянням прямої
    let yValues: number[] = xValues.map((x) => (-A * x - C) / B);

    //console.log({xValues}, yValues);

    return [xValues, yValues];
  }

  // побудова дзеркального відображення трикутника
  buildMirroredTriangle(line: [number[], number[]], triangle: Vertex[]) {
    //координати 1 точки прямої
    let p0X = line[0][0];
    let p0Y = line[1][0];
    //координати 2 точки прямої
    let p1X = line[0][1];
    let p1Y = line[1][1];
    let px, py, dx, dy, a, b, x, y;
    const result: Vertex[] = [];

    for (let i = 0; i < triangle.length; i++) {
      px = triangle[i].x
      py = triangle[i].y
      dx = p1X - p0X;
      dy = p1Y - p0Y;
      a = (dx * dx - dy * dy) / (dx * dx + dy * dy);
      b = 2 * dx * dy / (dx * dx + dy * dy);
      x = a * (px - p0X) + b * (py - p0Y) + p0X; //Math.round
      y = b * (px - p0X) - a * (py - p0Y) + p0Y; //Math.round
      result.push(new Vertex(x, y));
    }
    return this.prepareTriangleData(result);
  }

  private prepareTriangleData(triangle: Vertex[]): [number[], number[]] {
    let triangleX :number[] = [triangle[0].x, triangle[1].x, triangle[2].x, triangle[0].x];
    let triangleY :number[] = [triangle[0].y, triangle[1].y, triangle[2].y, triangle[0].y];
    return [triangleX, triangleY];
  }
}
