import { Injectable } from '@angular/core';

declare let Plotly: any;
@Injectable({
  providedIn: 'root'
})
export class PlotlyService {

  constructor() {
  }

  plotLine(title: string, plotDiv: string, originalTriangleData : [number[], number[]], lineData: [number[], number[]], mirroredTriangleData : [number[], number[]]){
    let trace1 = {
      x: originalTriangleData.at(0),
      y: originalTriangleData.at(1),
      type: 'rect',
      mode: 'lines',
      name: 'triangle (original)',
    };

    let trace2 = {
      x: lineData.at(0),
      y: lineData.at(1),
      type: 'rect',
      mode: 'lines',
      name: 'line'
    };

    let trace3 = {
      x: mirroredTriangleData.at(0),
       y: mirroredTriangleData.at(1),
       type: 'rect',
       mode: 'lines',
       name: 'triangle (mirrored)'
     };

    let layout = {
      //width: 100,
      //height: 700,
      xaxis: {
        title: {
          text: "Y",
          standoff: 20
        },
        range: [ -10, 10 ]
      },
      yaxis: {
        title: {
          text: "X",
          standoff: 20
        },
        range: [-10, 10]
      },
      title:title
    };

    Plotly.newPlot(plotDiv, [trace1, trace2, trace3], layout);
  }
}
