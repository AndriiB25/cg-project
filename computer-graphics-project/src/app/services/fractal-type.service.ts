import { Injectable } from '@angular/core';
import {FractalType} from "../models/fractal-type";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FractalTypeService {
  private fractalTypeSubject = new BehaviorSubject<FractalType>(FractalType.Koch); // Initialize with an empty string
  fractalType$ = this.fractalTypeSubject.asObservable();

  setFractalType(type: FractalType) {
    this.fractalTypeSubject.next(type);
  }

  constructor() { }
}
