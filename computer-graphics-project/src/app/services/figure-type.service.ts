import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {FigureType} from "../models/figure-type";

@Injectable({
  providedIn: 'root'
})

//цей сервіс допомогає пересилати дані між компонентами
// через механізм
export class FigureTypeService {

  //предмет RxJS, який може зберігати та видавати значення
  private figureTypeSubject = new BehaviorSubject<FigureType>(FigureType.Triangle);
  // Це дозволяє іншим компонентам підписуватися на зміни типу фігури.
  figureType$ = this.figureTypeSubject.asObservable();

  /*
  Цей метод приймає параметр типу, який представляє тип фігури.
  Він встановлює для figureTypeSubject нове значення типу, викликаючи .next(type) для BehaviorSubject.
  Ця дія сповіщає всіх підписників (компонентів) про те, що тип фігури змінився.
  */
  setFigureType(type: FigureType) {
    this.figureTypeSubject.next(type);
  }

  constructor() { }
}
