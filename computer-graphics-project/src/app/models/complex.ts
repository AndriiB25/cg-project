export class Complex {
    re: number;
    im: number;

    constructor(re: number, im: number) {
        this.re = re;
        this.im = im;
    }

    abs(): number {
        return Math.sqrt(this.re * this.re + this.im * this.im);
    }

    add(other: Complex): Complex {
        return new Complex(this.re + other.re, this.im + other.im);
    }

    mul(other: Complex): Complex {
        return new Complex(this.re * other.re - this.im * other.im,
            this.re * other.im + this.im * other.re);
    }

    tan(): Complex {
        const re = this.re;
        const im = this.im;

        let newRe = Math.sin(2 * re) / (Math.cos(2 * re) + Math.cosh(2 * im));
        let newIm = Math.sinh(2 * im) / (Math.cos(2 * re) + Math.cosh(2 * im));

        return new Complex(newRe, newIm);
    }

    cot(): Complex {
        const re = this.re;
        const im = this.im;

/*      let newRe = Math.sin(2 * re) / (Math.cos(2 * im) - Math.cosh(2 * re));
        let newIm = Math.sinh(2 * im) / (Math.cos(2 * im) - Math.cosh(2 * re)); */

        let newRe = (Math.sin(2 * re)) / (Math.cosh(2 * im)  - Math.cos(2 * re));
        let newIm = ((Math.sinh(2 * im)) / (Math.cosh(2 * im)  - Math.cos(2 * re))) * (-1);

        return new Complex(newRe, -newIm);
    }
}
