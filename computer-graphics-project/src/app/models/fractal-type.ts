export enum FractalType {
  Koch,
  Tangent,
  Cotangent
}
