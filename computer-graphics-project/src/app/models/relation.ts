export class Relation {
    public first: number;
    public second: number;

    constructor(first: number, second: number)
    {
      this.first = first;
      this.second = second;
    }

    public calculate(): number {
      return this.first / this.second;
    }
}
