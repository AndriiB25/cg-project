export class SchemesOutputData {
    cmykString: string = "";
    hsvString: string = "";
    constructor(cmyk: string, hsv: string) {
        this.cmykString = cmyk;
        this.hsvString = hsv;
    }
}
