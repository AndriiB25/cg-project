import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FigureTypeService} from "../../../services/figure-type.service";
import {FormsModule} from "@angular/forms";
import {FigureType} from "../../../models/figure-type";

@Component({
  selector: 'app-figure-type-switch',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './figure-type-switch.component.html',
  styleUrls: ['./figure-type-switch.component.css']
})
export class FigureTypeSwitchComponent {
  isChecked : boolean = false;

  constructor(private figureTypeService: FigureTypeService) { }

  updateFigureType() {
    const figureType = this.isChecked ? FigureType.Square : FigureType.Triangle;
    this.figureTypeService.setFigureType(figureType);
  }
}
