import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FractalTypeService} from "../../../services/fractal-type.service";
import {FractalType} from "../../../models/fractal-type";

@Component({
  selector: 'app-fractal-type-selector',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './fractal-type-selector.component.html',
  styleUrls: ['./fractal-type-selector.component.css']
})
export class FractalTypeSelectorComponent {
  public isKoch: boolean = true;
  isTangent: boolean = false;
  isCotangent: boolean = false;

  public setType(event: any) {
    this.isKoch = event.target.value === "1";
    this.isTangent = event.target.value === "2";
    this.isCotangent = event.target.value === "3";
    let fractalType = this.checkFractalType();
    this.fractalTypeService.setFractalType(fractalType);
  }

  private checkFractalType()
  {
    if(this.isTangent) {
      return FractalType.Tangent;
    }
    if(this.isCotangent) {
      return FractalType.Cotangent
    }

    return FractalType.Koch;
  }

  constructor(private fractalTypeService: FractalTypeService) {
  }
}
