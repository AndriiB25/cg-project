import {Component, Input} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';

@Component({
  selector: 'app-menu-block',
  standalone: true,
    imports: [CommonModule, NgOptimizedImage],
  templateUrl: './menu-block.component.html',
  styleUrls: ['./menu-block.component.css']
})
export class MenuBlockComponent {
  @Input() imageSource : string = "";
  @Input() menuTitle: string = "";
  @Input() description : string = "";

  constructor() {
  }
}
