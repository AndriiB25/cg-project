import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-side-panel',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="side-panel col">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./side-panel.component.css']
})
export class SidePanelComponent {

}
