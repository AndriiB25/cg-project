import {Component, EventEmitter, Input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-save-button',
  standalone: true,
  imports: [CommonModule],
  template: `
    <button type="button" [disabled]="isDisabled" class="btn save-button btn-lg" (click)="save()">Save</button>
  `,
  styleUrls: ['./save-button.component.css']
})
export class SaveButtonComponent {
  @Input() isDisabled = true;
  @Output() saveEvent = new EventEmitter<void>();
  save() {
    this.saveEvent.emit();
  }
}
