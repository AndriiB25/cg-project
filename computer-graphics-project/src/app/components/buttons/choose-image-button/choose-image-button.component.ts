import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import { faFileImport } from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {MatTooltipModule} from "@angular/material/tooltip";

@Component({
  selector: 'app-choose-image-button',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, FontAwesomeModule, MatTooltipModule],
  template: `
    <input type="file" class="file-input" accept="image/*"
          (change)="onFileSelected($event)" #fileUpload>

    <div class="file-upload mt-4 mb-4 d-flex justify-content-center align-items-center flex-column">

      <p class="fileName-info mb-2">{{fileName || "No file uploaded yet"}}</p>

      <button mat-raised-button color="primary" class="upload-btn"
              (click)="fileUpload.click()">
              <fa-icon [icon]="faFileImport" class="file-import"></fa-icon>
        <span class="button-text">Choose file</span>
      </button>
    </div>
  `,
  styleUrls: ['./choose-image-button.component.css']
})
export class ChooseImageButtonComponent {
  faFileImport = faFileImport ;
  @Output() imageChangedEvent = new EventEmitter<HTMLImageElement>();
  fileName = '';

  imageSource: HTMLImageElement = new Image();
  constructor() {}

  onFileSelected(event : Event) {
    const inputElement = event.target as HTMLInputElement;
    if (inputElement.files && inputElement.files.length > 0) {
      const selectedFile = inputElement.files[0];
      this.fileName = selectedFile.name;

      this.imageSource.src = URL.createObjectURL(selectedFile);
    } else {
      this.fileName = 'No file uploaded yet.';
      this.imageSource.src = '';
    }

    this.imageChangedEvent.emit(this.imageSource);
  }
}
