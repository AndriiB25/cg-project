import {Component, EventEmitter, Input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-clear-button',
  standalone: true,
  imports: [CommonModule],
  template: `
    <button type="button" [disabled]="isDisabled" class="btn clear-button btn-lg" (click)="clearSomething()">Clear</button>
  `,
  styleUrls: ['./clear-button.component.css']
})
export class ClearButtonComponent {
  @Input() isDisabled: boolean = true;
  @Output() clearSomethingEvent = new EventEmitter<void>();
  clearSomething() {
    this.clearSomethingEvent.emit();
  }
}
