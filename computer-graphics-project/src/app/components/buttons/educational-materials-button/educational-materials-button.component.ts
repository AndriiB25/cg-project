import { Component } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-educational-materials-button',
  standalone: true,
  imports: [CommonModule, RouterLink, NgOptimizedImage],
  template: `
    <div class="educational-materials-block d-flex  align-items-center">
      <img src="assets/images/educational-materials.png" alt="icon" >
      <!-- <fa-icon [icon]="faArrowLeft" class="arrow-left-dark"></fa-icon> -->
      <p class="sidebar-text fs-2">Educational Materials</p>
    </div>
  `,
  styleUrls: ['./educational-materials-button.component.css']
})
export class EducationalMaterialsButtonComponent {

}
