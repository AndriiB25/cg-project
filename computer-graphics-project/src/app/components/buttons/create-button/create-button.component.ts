import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  standalone : true,
  selector: 'app-create-button',
  template: `
    <button type="button" class="btn create-button btn-lg" (click)="createSomething()">Create</button>
  `,
  styleUrls: ['./create-button.component.css']
})
export class CreateButtonComponent {
  @Output() createSomethingEvent = new EventEmitter<void>();

  public createSomething() {
    this.createSomethingEvent.emit();
  }
}
