import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-line-parameters-input',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './line-parameters-input.component.html',
  styleUrls: ['./line-parameters-input.component.css']
})

export class LineParametersInputComponent {
    @Output() lineParamsChanged = new EventEmitter<[number, number, number]>();

    firstParam: number = 0;
    secondParam: number = 0;
    thirdParam: number = 0;

    emitLineParametersValues() {
        this.lineParamsChanged.emit([this.firstParam, this.secondParam, this.thirdParam]);
    }

}
