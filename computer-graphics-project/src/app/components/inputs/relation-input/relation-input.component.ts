import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {Relation} from "../../../models/relation";

@Component({
  selector: 'app-relation-input',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './relation-input.component.html',
  styleUrls: ['./relation-input.component.css']
})

export class RelationInputComponent {
    @Output() relationValueChangedEvent = new EventEmitter<Relation>();
    numberOfFirstRelation: number = 1;
    numberOfSecondRelation: number = 1;

    emitRelationValues() {
      let relation = new Relation(this.numberOfFirstRelation, this.numberOfSecondRelation);
      this.relationValueChangedEvent.emit(relation);
    }
}
