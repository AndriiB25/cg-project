import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {Vertex} from "../../../models/vertex";

@Component({
  selector: 'app-vertex-input',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './vertex-input.component.html',
  styleUrls: ['./vertex-input.component.css']
})

export class VertexInputComponent {
    @Output() vertexValueChanged = new EventEmitter<[Vertex, Vertex]>();

    firstX: number = 0;
    firstY: number = 0;
    secondX: number = 0;
    secondY: number = 0;

    emitVertexValues() {
      this.vertexValueChanged.emit(
        [new Vertex(this.firstX, this.firstY),
          new Vertex(this.secondX, this.secondY)]);
    }
}
