import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-saturation-slider',
  standalone: true,
  imports: [CommonModule,FormsModule],
  template: `
    <div class="saturation-block d-flex flex-column align-items-center">
      <p class="sidebar-text mt-3 mb-2 fs-2">Saturation</p>
      <input type="range" class="form-range" (input)="onSaturationChange()" (change)="onSaturationChange()" [(ngModel)]="saturationValue" min="0" max="100" step="1" id="zoom">
    </div>
  `,
  styleUrls: ['./saturation-slider.component.css']
})
export class SaturationSliderComponent {
    saturationValue: number = 0;
    @Output() valueChangedEvent = new EventEmitter<number>();

    constructor() {
    }

    onSaturationChange() {
        this.valueChangedEvent.emit(this.saturationValue / 100);
    }
}
