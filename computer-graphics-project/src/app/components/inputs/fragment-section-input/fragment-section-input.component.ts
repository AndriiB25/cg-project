import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {Fragment} from "../../../models/fragment";
import {ResetValuesService} from "../../../services/reset-values.service";

@Component({
  selector: 'app-fragment-section-input',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './fragment-section-input.component.html',
  styleUrls: ['./fragment-section-input.component.css']
})

export class FragmentSectionInputComponent implements OnInit {
  X: number = 0;
  Y: number = 0;
  Height: number = 0;
  Width: number = 0;

  @Output() inputChanged = new EventEmitter<Fragment>();
  emitFragmentValues() {
    this.inputChanged.emit(new Fragment(this.X, this.Y, this.Height, this.Width));
  }

  constructor(private inputCleared: ResetValuesService) {
  }

  ngOnInit(): void {
    this.inputCleared.clearInput$.subscribe((value: number) => {
      this.X = 0;
      this.Y = 0;
      this.Width = 0;
      this.Height = 0;
    })
  }

}
