import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-iterations-input',
  standalone: true,
  imports: [CommonModule, FormsModule],
  template: `
    <div class="iteration-block d-flex flex-column align-items-center">
      <p class="sidebar-text mt-3 mb-2 fs-2">Iterations</p>
      <input type="number" [attr.min]="0"  (mouseleave)="emitIterationValue()" [(ngModel)]="numberOfIterations" class="form-control" id="numOfIteration">
    </div>
  `,
  styleUrls: ['./iterations-input.component.css']
})

export class IterationsInputComponent {
  @Output() iterationValueChangedEvent = new EventEmitter<number>();
  numberOfIterations: number = 0;

  emitIterationValue() {
    this.iterationValueChangedEvent.emit(this.numberOfIterations);
  }
}
