import {Component, EventEmitter, Output} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-length-altitude-input',
  standalone: true,
  imports: [CommonModule,FormsModule],
  template: `
    <div class="d-flex flex-column align-items-center">
      <p class="main-label mt-3 mb-2">Length of altitude:</p>
      <input type="number" min="0" max="10" class="form-control" (mouseleave)="emitLengthValue()"
             [(ngModel)]="numberOfLength">
    </div>
  `,
  styleUrls: ['./length-altitude-input.component.css']
})
export class LengthAltitudeComponent {
    @Output() lengthValueChanged = new EventEmitter<number>();
    numberOfLength: number = 0;

    emitLengthValue() {
      this.lengthValueChanged.emit(this.numberOfLength);
    }
}
