import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ColorSchemesService} from "../../../services/color-schemes.service";
import {SchemesOutputData} from "../../../models/schemes-output-data";
import {Fragment} from "../../../models/fragment";
import {ResetValuesService} from "../../../services/reset-values.service";

@Component({
  selector: 'app-colored-canvas',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="image-block col p-2 d-flex justify-content-around align-items-center">
      <div id="outputImage" class="d-flex justify-content-around align-items-center">
        <canvas #canvasElement [width]="width" [height]="height" (mousemove)="onMouseMove($event)"></canvas>
      </div>
    </div>
  `,
  styleUrls: ['./colored-canvas.component.css']
})
export class ColoredCanvasComponent implements AfterViewInit  {
  width = 900;
  height = 600;
  isCanvasEmpty = true;

  private imageUrl = "";
  private currentSaturationValue: number = 0;
  private fragmentValue: Fragment = new Fragment(0, 0, 0, 0);
  @Input("image") imageToView = new Image();
  @Output() imageReset = new EventEmitter<void>();
  @ViewChild('canvasElement', {static: false}) private canvasElement: ElementRef<HTMLCanvasElement>
    = {} as ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  ngAfterViewInit(): void {
    // @ts-ignore
      this.ctx = this.canvasElement.nativeElement.getContext("2d");
  }

  constructor(private colorSchemesService: ColorSchemesService,
              private resetInputService: ResetValuesService) {}

  setImage(value: HTMLImageElement) {
    if(this.imageUrl != "") {
      this.clear();
    }

    this.imageToView = value;
    this.imageUrl = this.imageToView.src;

    // @ts-ignore
    this.imageToView.onload = () => {
      this.putImage();
    }
  }

  setSaturation(value: number) {
    value = Math.round(value * 100);
    this.currentSaturationValue = value;
    this.change();
  }


  setFragment(value: Fragment) {
    this.fragmentValue = value;
  }

  onMouseMove(event: MouseEvent) {
    let pos = this.getMousePosition(event);
    let rgb = this.colorSchemesService.getRgbPixel(this.canvasElement.nativeElement, pos.x, pos.y);
    let hsv = this.colorSchemesService.rgb_to_hsv(rgb.r, rgb.g, rgb.b);
    let cmyk = this.colorSchemesService.rgb_to_cmyk(rgb.r, rgb.g, rgb.b);

    let hsvStr = "HSV: " +
        (hsv.h).toFixed(0) +
        "°, " +
        hsv.s.toFixed(3) +
        ", " +
        hsv.v.toFixed(3);
    let cmykStr = "CMYK: "+
        cmyk.c.toFixed(3) +
        ", "+ cmyk.m.toFixed(3) +
        ", " + cmyk.y.toFixed(3) +
        ", " + cmyk.k.toFixed(3);

    let outputData = new SchemesOutputData(cmykStr, hsvStr);
    this.colorSchemesService.setOutputData(outputData);
  }

  saveAsImage() {
    let canvasUrl = this.canvasElement.nativeElement.toDataURL();
    const createEl = document.createElement('a');
    createEl.href = canvasUrl;

    createEl.download = 'result';

    createEl.click();
    createEl.remove();
  }

  private getMousePosition(evt: MouseEvent) {
    let rect = this.canvasElement.nativeElement.getBoundingClientRect();
    return {
      x: Math.round(evt.clientX - rect.left),
      y: Math.round(evt.clientY - rect.top)
    };
  }

  clear() {
    this.ctx.clearRect(0, 0, this.canvasElement.nativeElement.width, this.canvasElement.nativeElement.height);
    this.isCanvasEmpty = true;
    this.imageToView = new Image();
    this.imageToView.src = this.imageUrl;
    this.currentSaturationValue = 0;
    this.imageReset.emit();
    this.resetInputService.reset(0);
  }

  private putImage() {
    if (this.imageToView.width > this.imageToView.height) {
      this.ctx.drawImage(
        this.imageToView,
          0,
          0,
          this.imageToView.width,
          this.imageToView.height,
          0,
          0,
          this.canvasElement.nativeElement.width,
          this.canvasElement.nativeElement.height / (this.imageToView.width / this.imageToView.height)
      );
      } else {
        this.ctx.drawImage(
          this.imageToView,
            0,
            0,
            this.imageToView.width,
            this.imageToView.height,
            0,
            0,
            this.canvasElement.nativeElement.width / (this.imageToView.height / this.imageToView.width),
            this.canvasElement.nativeElement.height
          );
      }
    this.isCanvasEmpty = false;
  }

  private change() {
    this.colorSchemesService.changeColors(
      this.canvasElement.nativeElement,
      this.fragmentValue,
      this.currentSaturationValue);
  }
}
