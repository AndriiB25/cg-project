import {Component} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PlotlyService} from "../../../services/plotly.service";
import {Vertex} from "../../../models/vertex";
import {FiguresDrawingService} from "../../../services/figures-drawing.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-transformation-canvas',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="axe-block col p-2 d-flex justify-content-around align-items-center">
      <div class="axe-inner-block d-flex justify-content-around align-items-center">
        <div id="outputAxe"></div>
      </div>
    </div>
  `,
  styleUrls: ['./transformation-canvas.component.css']
})

export class TransformationCanvasComponent {
  width = 900;
  height = 600;

  triangleAltitudeLength = 0;
  firstVertex : Vertex = new Vertex(0,0);
  secondVertex: Vertex = new Vertex(0,0);

  A = 0; B = 0; C = 0;
  isPlotEmpty = true;

  private divName = "outputAxe"
  constructor(private plotlyService: PlotlyService,
              private figuresDrawingService: FiguresDrawingService,
              private toastr: ToastrService) {
  }

  setVertexes(pair: [Vertex, Vertex]) {
    this.firstVertex = pair[0];
    this.secondVertex = pair[1];
  }

  setAltitudeLength(value: number) {
    this.triangleAltitudeLength = value;
  }

  setLinePoints(values: [number, number, number]) {
    this.A = values[0];
    this.B = values[1];
    this.C = values[2];
  }

  draw() {
    if(!this.isDataValid()) {
      return;
    }

    const firstBaseVertex: Vertex = { x: this.firstVertex.x, y: this.firstVertex.y };
    const secondBaseVertex: Vertex = { x: this.secondVertex.x, y: this.secondVertex.y };
    const altitudeLength: number = this.triangleAltitudeLength;

    const [originalTriangle, originalTriangleData] =
        this.figuresDrawingService.buildIsoscelesTriangle(firstBaseVertex, secondBaseVertex, altitudeLength);
    const lineData = this.figuresDrawingService.buildLine(this.A, this.B, this.C);
    const mirroredTriangleData = this.figuresDrawingService.buildMirroredTriangle(lineData, originalTriangle);

    this.plotlyService.plotLine("", this.divName, originalTriangleData, lineData, mirroredTriangleData);
    this.isPlotEmpty = false;
  }

  clear() {
    this.plotlyService.plotLine("", this.divName, [[],[]], [[],[]], [[],[]]);
    this.isPlotEmpty = true;
  }

  private isDataValid() : boolean {
    let warningMessage = this.getWarningMessage();

    if(warningMessage !== "") {
      this.toastr.info(warningMessage, "Attention!", {
        positionClass: 'toast-bottom-right',
        timeOut: 5000, // (ms)
      });
      return false;
    }

    return true;
  }

  private getWarningMessage() : string {
    if(this.firstVertex.x === this.secondVertex.x && this.firstVertex.y == this.secondVertex.y) {
      return "Please, enter valid base axes!";
    }

    if(this.triangleAltitudeLength ===0) {
      return "Please, enter altitude data";
    }

    if(this.A === 0 && this.B === 0 && this.C === 0) {
      return "Please, enter line data";
    }

    return "";
  }
}




