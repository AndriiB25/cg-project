import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FigureTypeService} from "../../../services/figure-type.service";
import {FigureType} from "../../../models/figure-type";
import {FractalsDrawingService} from 'src/app/services/fractals-drawing.service';
import {FractalType} from "../../../models/fractal-type";
import {FractalTypeService} from "../../../services/fractal-type.service";
import {ToastrService} from "ngx-toastr";
import {Relation} from "../../../models/relation";


@Component({
    standalone: true,
    selector: 'app-canvas-drawing',
    template: `
        <div class="fractal-block col p-2 d-flex justify-content-around align-items-center">
            <div id="outputFractal" class="d-flex justify-content-around align-items-center">
                <canvas #canvasElement width="{{width}}" height="{{height}}" (wheel)="changeScale($event)"></canvas>
            </div>
        </div>
    `,
    styleUrls: ['./canvas-drawing.component.css']
})
export class CanvasDrawingComponent implements AfterViewInit, OnInit {
  width = 900;
  height = 600;
  visibleWidth = this.width;
  visibleHeight = this.height;
  scale = 1;
  originX = 0;
  originY = 0;
  isCanvasEmpty = true;

  private clearOffset = 200;

  @Input() relation: Relation;
  @Input() iterations: number = 0;

  get canvasElement(): ElementRef {
    return this._canvasElement;
  }

  get ctx(): CanvasRenderingContext2D {
    return this._ctx;
  }

  @ViewChild('canvasElement', {static: false}) private _canvasElement: ElementRef<HTMLCanvasElement>
    = {} as ElementRef<HTMLCanvasElement>;

  private _ctx: CanvasRenderingContext2D;
  private figureType: FigureType = FigureType.Triangle;
  private fractalType: FractalType = FractalType.Koch;

  constructor(private figureTypeService: FigureTypeService,
              private fractalsDrawingService: FractalsDrawingService,
              private fractalTypeService: FractalTypeService,
              private toastr: ToastrService) {}

  ngOnInit(): void {
    this.figureTypeService.figureType$.subscribe(type => {
      this.figureType = type;
    } );

    this.fractalTypeService.fractalType$.subscribe(type => this.fractalType = type);
  }

  public changeScale(event: WheelEvent) {
    this.clear();
    const zoomIntensity = 0.3;
    const mouseX = event.clientX - this.canvasElement.nativeElement.offsetLeft;
    const mouseY = event.clientY - this.canvasElement.nativeElement.offsetTop;
    const wheel = event.deltaY < 0 ? 1 : -1;

    const zoom = Math.exp(wheel * zoomIntensity);

    this.ctx.translate(this.originX, this.originY);

    this.originX -= mouseX / (this.scale * zoom) - mouseX / this.scale;
    this.originY -= mouseY / (this.scale * zoom) - mouseY / this.scale;

    this.ctx.scale(zoom, zoom);

    this.ctx.translate(-this.originX, -this.originY);

    this.scale *= zoom;
    this.visibleWidth = this.width / this.scale;
    this.visibleHeight = this.height / this.scale;
    this.draw();
  }

  ngAfterViewInit(): void {
    this._ctx = this.canvasElement.nativeElement.getContext("2d");
  }

  clear() {
    this._ctx.clearRect(0, 0, this._canvasElement.nativeElement.width + this.clearOffset, this._canvasElement.nativeElement.height + this.clearOffset);
    this.isCanvasEmpty = true;
  }

  draw() {
    if(this.iterations > 1000)
    {
      this.toastr.warning("Are you mad!?", "Attention!", {
        positionClass: 'toast-bottom-right',
        timeOut: 5000, // (ms)
      });
      return;
    }

    this.clear();

    if(this.relation.calculate() >= 0.5 && this.fractalType == FractalType.Koch) {
      this.toastr.warning("Relation value must be less than 0,5 to work correctly", "Attention!", {
        positionClass: 'toast-bottom-right',
        timeOut: 5000, // (ms)
      });
      return;
    }

    switch (this.fractalType) {
      case FractalType.Koch: {
        this.handleForKochType();
        break;
      }

      case FractalType.Tangent: {
        this.fractalsDrawingService.drawUsingTangentOrCotangent(true, this._ctx, 900, 600, this.iterations);
        break;
      }

      case FractalType.Cotangent: {
        this.fractalsDrawingService.drawUsingTangentOrCotangent(false, this._ctx, 900, 600, this.iterations);
        break;
      }
    }

    this.isCanvasEmpty = false;
  }

  saveAsImage() {
    let canvasUrl = this.canvasElement.nativeElement.toDataURL();
    const createEl = document.createElement('a');
    createEl.href = canvasUrl;

    createEl.download = this.getDownloadFileName();

    createEl.click();
    createEl.remove();
  }

  private handleForKochType()
  {
    const maxIterations = 6;
    if(this.iterations > maxIterations) {
      this.toastr.info("You can't enter more than 6 iterations for this type of fractal", "Attention!", {
        positionClass: 'toast-bottom-right',
        timeOut: 5000, // (ms)
      });
      return;
    }

    if(this.figureType == FigureType.Triangle) {
      this.fractalsDrawingService.drawKoch(this.iterations, 3, this.relation.calculate(), this._ctx, this.width, this.height);
    } else {
      this.fractalsDrawingService.drawKoch(this.iterations, 4, this.relation.calculate(), this._ctx, this.width, this.height);
    }
  }

  private getDownloadFileName() {
    let name: string;
    switch (this.fractalType)
    {
      case FractalType.Koch:
      {
        name = `${this.figureType == FigureType.Square? 'square': 'triangular'}-koch`;
        break;
      }
      case FractalType.Tangent: name = 'tangent'; break;
      case FractalType.Cotangent: name = 'cotangent'; break;
    }

    return `${name}-fractal`;
  }
}
