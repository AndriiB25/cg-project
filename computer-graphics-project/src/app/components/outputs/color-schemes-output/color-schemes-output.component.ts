import {Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ColorSchemesService} from "../../../services/color-schemes.service";
import {SchemesOutputData} from "../../../models/schemes-output-data";

@Component({
  selector: 'app-color-schemes-output',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="mt-4">
      <p class="label-text">{{outputData.hsvString}}</p>
      <p class="label-text">{{outputData.cmykString}}</p>
    </div>
  `,
  styleUrls: ['./color-schemes-output.component.css']
})
export class ColorSchemesOutputComponent implements OnInit {


  outputData: SchemesOutputData = new SchemesOutputData("", "");
  constructor(private colorSchemesService: ColorSchemesService) {
  }

  ngOnInit() {
    this.colorSchemesService.schemesData$.subscribe((result : SchemesOutputData) => {
      this.outputData = result;
    })
  }

}
