import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {FractalsComponent} from "./pages/fractals/fractals.component";
import {ColorsComponent} from "./pages/colors/colors.component";
import {TransformationComponent} from "./pages/transformation/transformation.component";
import {FirstEducationalComponent} from "./pages/educational/first-educational/first-educational.component";
import {SecondEducationalComponent} from "./pages/educational/second-educational/second-educational.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'fractals', component: FractalsComponent },
  { path: 'colors', component: ColorsComponent },
  { path: 'transformation', component: TransformationComponent },
  { path: 'educational-1', component: FirstEducationalComponent },
  { path: 'educational-2', component: SecondEducationalComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
